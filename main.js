
window.onload = function() {
  // DOM
  var boardRoot = document.getElementById('board');
  var reset = document.getElementById('reset');
  var status = document.getElementById('status');

  // execution
  var g = new Game(boardRoot, status);
  g.renderBoard();

  // events
  boardRoot.addEventListener('click', e => g.handleClick(e));
  reset.addEventListener('click', () => g.reset());
}
