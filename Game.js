// Constants
var NEW_BOARD = [
  [' ',' ',' '],
  [' ',' ',' '],
  [' ',' ',' '],
];

// array helpers
Array.prototype.equal = function(a2) {
  return JSON.stringify(this) == JSON.stringify(a2);
}
Array.prototype.includes2d = function(needle) {
  for (let i = 0; i < this.length; i++) {
    if (this[i].includes(needle)) {
      return true;
    }
  }
  return false;
}

// Tic-Tac-Toe Game
class Game {
  constructor(root, status) {
    // game state
    this.board = NEW_BOARD.map(r=>r.slice());	
    this.gameOver = false;
    this.xTurn = true;
    // DOM
    this.status = status;
    this.root = root;
  }

  renderBoard() {
    var trs = Array.from(this.root.children[0].children);
    var cells = trs.map(row => Array.from(row.children));
    for (let y = 0; y < this.board.length; y++) {
      for (let x = 0; x < this.board[y].length; x++) {
        cells[y][x].innerHTML = this.board[y][x];
      }
    }
  }

  insert(y, x, c) {
    if (this.board[y][x] === ' ') {
      this.board[y][x] = c;
      this.xTurn = !this.xTurn;
    }
  }

  handleClick(e) {
    var coords = e.target.id.split(',');
    var newTurn = this.xTurn ? 'X' : 'O';
    if (!this.gameOver && coords.length === 2) {
      this.insert(coords[0], coords[1], newTurn);
      this.renderBoard();
      this.checkGame();
    }
  }

  checkGame() {
    this.checkRows();
    this.checkCols();
    this.checkDiags();
    this.checkDraw();
  }
  checkDraw() {
    if (!this.board.includes2d(' ')) {
      this.status.innerHTML = 'Draw!';
      this.gameOver = true;
    }
  }
  checkDiags() {
    var diags = [
      this.board.map((r,i)=>r[i]),
      this.board.slice().reverse().map((r,i)=>r[i]),
    ];
    this.check(diags);
  }
  checkCols() {
    var cols = this.board.map((_,i)=>this.board.map(r=>r[i]));
    this.check(cols);
  }
  checkRows() {
    var rows = this.board.map(row=>row.slice());
    this.check(rows); 
  }
  check(slices) {
    for (let i = 0; i < slices.length; i++) {
      var size = slices[i].length;
      if (slices[i].equal([...'X'.repeat(size)])) {
        this.status.innerHTML = 'X wins!';
        this.gameOver = true;
      }
      else if (slices[i].equal([...'O'.repeat(size)])) {
        this.status.innerHTML = 'O wins!';
        this.gameOver = true;
      }
    }
  }
  reset() {
    // game state
    this.board = NEW_BOARD.map(r=>r.slice());	
    this.gameOver = false;
    this.xTurn = true;
    // DOM
    this.status.innerHTML = '';
    this.renderBoard();
  }
}